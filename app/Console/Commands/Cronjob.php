<?php

namespace App\Console\Commands;

use App\Http\Controllers\CronController;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

class Cronjob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob {--reset=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'start synchronization of cronjob for API';

    /**
     * Execute the console command.
     * cronjob [option reset]
     * @return mixed
     */
    public function handle()
    {
        $connected      = @fsockopen("api.football-data.org",80);
        $cronjob        = \App\Cronjob::where('id',1)->first();
        $crontroller    = new CronController($this,$cronjob);

        if($connected) {
            if(empty($this->option('reset')) || is_null($this->option('reset'))) {
                $crontroller->startCron();
            }
            else {
                $this->info('Resetted Task ');
                $cronjob->switchOff($crontroller);
            }
        }


    }
}
