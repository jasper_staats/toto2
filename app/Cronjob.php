<?php

namespace App;

use App\Http\Controllers\CronController;
use Illuminate\Database\Eloquent\Model;

class Cronjob extends Model
{
    protected $table = 'cronjob';
    protected $fillable = ['status', 'requests', 'time_reset'];

    public function switchStatus()
    {
        $this->status = ($this->status == 1) ? 0 : 1;
        $this->update();
    }

    public function switchOn(CronController $cron)
    {
        $cron->logInfo('CRON::ON');
        $this->status =  1;
        $this->update();
    }

    public function switchOff(CronController $cron)
    {
        $cron->logInfo('CRON::OFF');
        $this->status =  0;
        $this->update();
    }

    public function isBusy(){
        return $this->status;
    }
}
