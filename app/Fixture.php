<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fixture extends Model
{
    protected $table = 'fixtures';
    protected $fillable = ['id','soccerseasonId','date','status','matchday','homeTeamName','awayTeamName','result_goalsHomeTeam','result_goalsAwayTeam','homeTeamId','awayTeamId'];
}
