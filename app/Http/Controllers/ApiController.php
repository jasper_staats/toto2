<?php

namespace App\Http\Controllers;

use App\Cronjob;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class ApiController extends Controller
{
    protected $uri;
    protected $client;
    protected $console;
    protected $cron;
    protected $cronjob;
    protected $curloptions;

    public function __construct(Command $command, CronController $cron, Cronjob $cronjob)
    {
        $this->console      = $command;
        $this->cron         = $cron;
        $this->cronjob      = $cronjob;
        $this->curloptions  = ['curl.options' => array('CURLOPT_HTTP_VERSION' => 'CURL_HTTP_VERSION_1_0')];
        $this->client       = new Client($this->curloptions);
        $this->uri          = env('SOCCER_API_URL');
    }

    public function getSeasons($year = null)
    {
        if (!is_null($year) && $year > 2012) {
            $str = '/?season=' . $year;
        } else {
            $str = '';
        }

        return $this->request('soccerseasons' . $str);
    }

    public function getTeams($idSeason)
    {
        return $this->request('soccerseasons/' . $idSeason . '/teams');
    }

    public function getFixtures($idSeason = null)
    {
        $idSeason = (!is_null($idSeason)) ? 'soccerseasons/' . $idSeason . '/' : '';
        return $this->request('' . $idSeason . 'fixtures');
    }

    public function getPlayers($IdTeam)
    {
        return $this->request('teams/' . $IdTeam . '/players');
    }

    public function request($url)
    {
        if (is_null($url) || $url == '')
            return array();

        $uri = $this->uri . $url;
        $header = array('headers' => array('X-Auth-Token' => env('SOCCER_TOKEN'), 'X-Response-Control' => 'minified', 'http_errors' => false));
        $response = array();

        try {
            $response = $this->client->get($uri, $header, ['curl.options' => array('CURLOPT_HTTP_VERSION' => 'CURL_HTTP_VERSION_1_0')]);
            $counter = $response->getHeader('X-Requests-Available');
            $time = $response->getHeader('X-RequestCounter-Reset');

            // Update statistics cronjob
            if ($this->cronjob) {
                $this->cronjob->requests = $counter[0];
                $this->cronjob->time_reset = $time[0];
                $this->cronjob->update();
            }

            // Set the cronjob on hold if maximum requests has been used! - get the amount of time till reset of requests on API
            if ($counter[0] < 2) {
                $this->cron->logError('Maximum amount of requests , delaying cron for :' . $time[0] . 's');
                sleep($time[0] + 1);
            }

        } catch (RequestException $e) {
            $this->cron->logError('ERROR | ' . $e->getMessage());
        }

        return (!is_null($response)) ? json_decode($response->getBody()) : array();
    }

    public function getWiki($title)
    {
        try {
            $response = $this->client->get(sprintf(env('WIKI_URL'),$title),$this->curloptions);
            return json_decode($response->getBody());
        }
        catch (RequestException $e) {
            $this->cron->logError('ERROR | ' . $e->getMessage());
            return array();
        }

    }


    public function getClient()
    {
        return $this->client;
    }

    public function getUri()
    {
        return $this->uri;
    }

}
