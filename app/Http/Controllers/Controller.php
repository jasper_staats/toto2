<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Schema;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Returns a stripped object from the API as array and removes unncessary information for the DB
     * @param $object
     * @param Model $model
     * @return array
     */
    public function correctedObject($object , Model $model){

        $exceptions = (method_exists($model,'getExceptions')) ? $model->getExceptions() : array();
        $array = [];

        foreach($object as $key=>$value) {
            foreach($exceptions as $exception) {
                if($key == $exception || (!Schema::hasColumn($model->getTable(),$key))) {
                    unset($object->$key);
                }
                else {
                    $array[$key] = $value;
                }
            }
        }
        return $array;
    }
}
