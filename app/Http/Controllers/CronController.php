<?php

namespace App\Http\Controllers;

use App\Cronjob;
use Carbon\Carbon;
use Illuminate\Foundation\Inspiring;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CronController extends Controller
{
    protected $console;
    protected $cron;
    protected $api;
    protected $cronjob;

    public function __construct(Command $command , Cronjob $cronjob)
    {
        $this->console = $command;
        $this->cron = $this;
        $this->cronjob = $cronjob;
        $this->api = new ApiController($command, $this->cron , $cronjob);

    }

    public function startCron()
    {
        $seasonController   = new SeasonController($this->console, $this->api, $this->cron);
        $teamController     = new TeamController($this->console, $this->api, $this->cron);
        $fixtureController  = new FixtureController($this->console, $this->api, $this->cron);
        $playerController   = new PlayerController($this->console, $this->api, $this->cron);

        if ($this->cronAllowed()) {
            $this->cronjob->switchOn($this);
            $seasonController->sync();
            $teamController->sync();
            $fixtureController->sync();
            $playerController->sync();
            $this->cronjob->switchOff($this);

        }
        else {
            $this->logInfo('Provided task is already running');
        }
    }


    /* Log and write to command line */
    public function logInfo($msg)
    {
        $log = new Log();
        $this->console->info($msg);
        $log::info($msg);
    }

    /* Log and write to command line */
    public function logError($msg)
    {
        $log = new Log();
        $this->console->error($msg);
        $log::error($msg);
    }

    public function cronAllowed() {
        return (!$this->cronjob->isBusy() && $this->cronjob->requests > 0 );
    }
}

