<?php

namespace App\Http\Controllers;

use App\Fixture;
use App\Season;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FixtureController extends Controller
{
    protected $console;
    protected $cron;
    protected $api;

    public function __construct(Command $command, ApiController $api, CronController $cron)
    {
        $this->console = $command;
        $this->cron = $cron;
        $this->api = $api;
    }

    public function sync()
    {
        $seasons = Season::all();
        $this->cron->logInfo('Syncing Fixtures');
        foreach ($seasons as $season) {
            if ($season->state) {
                $this->console->info($season->id . ' - fixtures updated');
                $fixtures = $this->api->getFixtures($season->id);
                foreach ($fixtures->fixtures as $fixture) {
                    $fixture_db = Fixture::where('id', $fixture->id)->first();

                    if($fixture_db) {
                        $this->update($fixture,$fixture_db);
                        $this->cron->logInfo('Updating '. $fixture->id . ' ' . $fixture->homeTeamName  . ' - ' . $fixture->awayTeamName);
                    }
                    else {
                        $this->create($fixture);
                        $this->cron->logInfo('Adding '. $fixture->id . ' ' . $fixture->homeTeamName  . ' - ' . $fixture->awayTeamName);
                    }

                }
                // Give a signal that it isn't allowed to edit everything related to this season
                $season->state = 0;
                $season->save();
            }

        }

    }

    public function create($object)
    {
        $fields = array();

        foreach ($object as $key => $value) {

            if ($key != 'result') {
                $fields[$key] = $value;
            }

            if ($key == 'result') {
                foreach ($value as $k => $v) {
                    $fields['result_' . $k] = $v;
                }
            }

        }

        return Fixture::create($fields);

    }

    public function update($object, $object_db)
    {
        // Something has to be given back
        if (is_null($object) || is_null($object_db))
            return null;

        // For each season given as object, go through each field and fill the fields based on that!
        foreach ($object as $key => $value) {
            // Do not fill the _links field!

            if ($key != 'result') {
                $object_db->$key = $value;
            }
            if ($key == 'result') {
                foreach ($value as $k => $v) {
                    $str = 'result_' . $k ;
                    $object_db->$str = $v;
                }
            }

        }

        $object_db->save();
    }
}
