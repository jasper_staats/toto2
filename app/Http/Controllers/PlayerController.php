<?php

namespace App\Http\Controllers;

use App\Player;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Team;

class PlayerController extends Controller
{
    protected $console;
    protected $cron;
    protected $api;
    protected $teams;
    protected $model;

    public function __construct(Command $command , ApiController $api , CronController $cron)
    {
        $this->console      = $command;
        $this->cron         = $cron;
        $this->api          = $api;
        $this->teams        = Team::all();
        $this->model        = new Player();
    }

    public function sync()
    {
        $this->cron->logInfo('Syncing Players');
        $players = [];
        foreach ($this->teams as $team) {

           if ($team->status) {
                // Disable due too much requests unneccessary
               $players[$team->id] = $this->api->getPlayers($team->id); // Get all the players for each team

                // If the api doesn't see if there are any players, then skip this one
                if($players[$team->id]->count > 0) {

                    foreach($players[$team->id]->players as $player) {

                        $player->team = $team->id;

                        // Add images for each player
                        $wiki = $this->api->getWiki($player->name);

                        foreach($wiki->query->pages as $key=>$value) {
                            if(property_exists($wiki->query->pages->{$key},'thumbnail'))
                            {
                                $player->image = $wiki->query->pages->{$key}->thumbnail->source;
                            }
                        }
                        // End add images from wiki

                        $correctPlayer = parent::correctedObject($player,$this->model);
                        $player_db = Player::findOrNew($correctPlayer['id'])->fill($correctPlayer);

                        if($player_db) {
                            $player_db->update($correctPlayer);
                            $this->cron->logInfo('Updating ' . $player->name);
                        }


                    }
                }

                $team->status = 0;
                $team->save();
            }
        }

    }


}
