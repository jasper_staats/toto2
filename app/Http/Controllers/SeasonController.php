<?php

namespace App\Http\Controllers;

use App\Season;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Console\Command;

class SeasonController extends Controller
{

    protected $console;
    protected $cron;
    protected $api;

    public function __construct(Command $command, ApiController $api, CronController $cron)
    {
        $this->console = $command;
        $this->cron = $cron;
        $this->api = $api;
    }

    public function sync()
    {
        $this->cron->logInfo('Syncing Seasons');
        $currentSeasons = Season::all();

        if(!count($currentSeasons) > 0) {
            $years = array();
            $years[0] = 2013;
            $years[1] = 2014;
            $years[2] = 2015;
            $seasons = array();

            foreach ($years as $year) {
                $seasons[$year] = $this->api->getSeasons($year);
            }

            foreach($years as $year) {
                $selectSeason = $seasons[$year];

                foreach ($selectSeason as $season) {

                        $season_db = Season::where('id', $season->id)->first();

                        // If a season isn't found : then it's a new season
                        if ($season_db == null) {
                            $season->state = 1; // is new season! So sync that shizzle!
                            $this->create($season);

                            $this->cron->logInfo($season->id . ' - ' . $season->caption . ' added to DB');

                        } else {
                            // Compare updated date that we registrated and that the API has. If new, then update
                            // Plus we need both same time and date formate so format with Carbon
                            $updateDB = Carbon::parse($season_db->lastUpdated);
                            $updateDB->toW3cString();

                            $updateAPI = Carbon::parse($season->lastUpdated);
                            $updateAPI->toW3cString();

                            // If it isn't equal , then update , so edit
                            if ($updateAPI != $updateDB) {
                                $this->update($season, $season_db);
                                $this->cron->logInfo('Updated ' . $season->id . ' - ' . $season->caption);
                            }

                        }




                }

            }
        }
        else {
            $selectSeason = $this->api->getSeasons();

            foreach ($selectSeason as $season) {
                $season_db = Season::where('id', $season->id)->first();

                // If a season isn't found : then it's a new season
                if ($season_db == null) {
                    $season->state = 1; // is new season! So sync that shizzle!
                    $this->create($season);
                    $this->cron->logInfo($season->id . ' - ' . $season->caption . ' added to DB');

                } else {
                    // Compare updated date that we registrated and that the API has. If new, then update
                    // Plus we need both same time and date formate so format with Carbon
                    $updateDB = Carbon::parse($season_db->lastUpdated);
                    $updateDB->toW3cString();

                    $updateAPI = Carbon::parse($season->lastUpdated);
                    $updateAPI->toW3cString();

                    // If it isn't equal , then update , so edit
                    if ($updateAPI != $updateDB) {
                        $this->update($season, $season_db);
                        $this->cron->logInfo('Updated ' . $season->id . ' - ' . $season->caption);
                    }
                }

            }

        }







    }

    public function create($object)
    {
        $fields = array();
        foreach ($object as $key => $value) {
            // Do not fill the _links field!
            if ($key != '_links') {
                $fields[$key] = $value;
            }
        }

        Season::create($fields);
    }

    public function update($object, $object_db)
    {
        // Something has to be given back
        if (is_null($object) || is_null($object_db))
            return null;

        // For each season given as object, go through each field and fill the fields based on that!
        foreach ($object as $key => $value) {
            // Do not fill the _links field!
            if ($key != '_links') {
                $object_db->$key = $value;
            }
        }
        $object_db->state = 1;
        $object_db->save();
    }
}
