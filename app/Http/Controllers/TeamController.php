<?php

namespace App\Http\Controllers;

use App\Season;
use App\Team;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Console\Command;
use Carbon\Carbon;
use PhpParser\Builder\Class_;

class TeamController extends Controller
{
    protected $console;
    protected $cron;
    protected $api;
    protected $model;
    protected $seasons;

    public function __construct(Command $command, ApiController $api, CronController $cron)
    {
        $this->console      =  $command;
        $this->cron         =  $cron;
        $this->api          =  $api;
        $this->model        =  new Team();
        $this->seasons      =  Season::all();
    }

    /**
     * Sync for each team that a season has
     */
    public function sync()
    {
        $this->cron->logInfo('Syncing Teams');
        foreach ($this->seasons as $season) {

            if ($season->state) {
                $teams_season[$season->id] = $this->api->getTeams($season->id); // Get all the teams for each season
                $this->cron->logInfo($season->id . ' - ' . $season->name . ' teams are getting updated '); // send through message at console

                // Each team for each season has to be added or updated
                foreach ($teams_season[$season->id]->teams as $team) {

                    $team->season       = $season->id; // assign id of season to the model
                    $team->lastUpdated  = $season->lastUpdated; // give a definition of when it has been updated ( give the date of API as comparison )
                    $team->status       = 1;

                    $correctTeam = parent::correctedObject($team,$this->model);
                    $team_db = Team::findOrNew($correctTeam['id'])->fill($correctTeam);

                    if($team_db) {
                        $team_db->update($correctTeam);
                    }

                }

                $season->state = 0;
                $season->save();
            }
        }

    }

}
