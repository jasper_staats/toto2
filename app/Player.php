<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $table = 'players';
    protected $fillable = ['id','team','name','position','image','thumbnail','nationality','jerseyNumber','dateOfBirth','contractUntil','marketValue'];
    protected $exceptions =  ['_links'];

    public function getExceptions(){
        return $this->exceptions;
    }

    public function Team(){
        return $this->belongsTo('App\Team');
    }
}
