<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    protected $table = 'seasons';
    protected $fillable = ['id','caption','currentMatchday','league','numberOfTeams','lastUpdated','numberOfGames','numberOfMatchdays','year','state'];

    public function getId() {
        return $this->id;
    }

    public function getCaption(){
        return $this->caption;
    }

    public function Teams(){
        return $this->hasMany('Team');
    }


}
