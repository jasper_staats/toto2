<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';
    protected $fillable = ['id','season','shortName','name','status','crestUrl','lastUpdated','squadMarketValue'];
    protected $exceptions =  ['_links'];

    public function getExceptions(){
        return $this->exceptions;
    }
}
