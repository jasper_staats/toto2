<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixtures', function (Blueprint $table) {
            $table->integer('id');
            $table->integer('soccerseasonId');
            $table->dateTimeTz('date');
            $table->string('status')->nullable();
            $table->integer('matchday')->nullable();
            $table->string('homeTeamName');
            $table->string('awayTeamName');
            $table->integer('result_goalsHomeTeam')->nullable();
            $table->integer('result_goalsAwayTeam')->nullable();
            $table->integer('homeTeamId');
            $table->integer('awayTeamId');
            $table->dateTimeTz('lastUpdated');
            $table->dateTimeTz('lastSync');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fixtures');
    }
}
