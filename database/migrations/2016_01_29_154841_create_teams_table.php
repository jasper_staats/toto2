<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->integer('id',11);
            $table->integer('season');
            $table->string('name');
            $table->integer('status');
            $table->string('shortName')->nullable();
            $table->string('squadMarketValue')->nullable();
            $table->string('crestUrl')->nullable();
            $table->dateTimeTz('lastUpdated');
            $table->dateTimeTz('lastSync');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('teams');
    }
}
