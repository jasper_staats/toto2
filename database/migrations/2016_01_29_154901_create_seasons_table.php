<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seasons', function (Blueprint $table) {
            $table->integer('id');
            $table->string('caption');
            $table->string('league');
            $table->string('year');
            $table->integer('currentMatchday')->nullable();
            $table->integer('numberOfMatchdays');
            $table->integer('numberOfTeams');
            $table->integer('numberOfGames');
            $table->integer('state');
            $table->dateTimeTz('lastUpdated');
            $table->dateTimeTz('lastSync');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seasons');
    }
}
