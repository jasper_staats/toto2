<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->integer('id');
            $table->integer('team');
            $table->text('name')->nullable();
            $table->text('image')->nullable();
            $table->text('position')->nullable();
            $table->text('thumbnail')->nullable();
            $table->text('nationality')->nullable();
            $table->integer('jerseyNumber')->nullable();
            $table->date('dateOfBirth')->nullable();
            $table->date('contractUntil')->nullable();
            $table->text('marketValue')->nullable();
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('players');
    }
}
