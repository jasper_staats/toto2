<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cronjob', function (Blueprint $table) {
            $table->integer('id');
            $table->text('description');
            $table->integer('status');
            $table->integer('requests');
            $table->integer('time_reset');
            $table->dateTimeTz('lastUpdated');
            $table->dateTimeTz('lastSync');
            $table->rememberToken();
            $table->timestamps();
        });

        $cronjob = new \App\Cronjob();
        $cronjob->id = 1;
        $cronjob->description = 'cronjob - main';
        $cronjob->status = 0;
        $cronjob->requests = 100;
        $cronjob->time_reset = 60;
        $cronjob->save();


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cronjob');
    }
}
