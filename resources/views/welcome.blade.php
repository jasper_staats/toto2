<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
            font-weight: bold;
        }

        .teams {
            text-align: left;
            font-size: 1.2em;
            font-weight: bold;
            list-style: none;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="title">Teams</div>
    <div class="content">
        <?php $team = \App\Player::all()->sortByDesc('name') ?>
        <ul class="teams">
            @foreach($team as $t)
                            <li>{{$t->name}}</li>
                @if($t->image != '' || !is_null($t->image) )
                            <li><img width="150px" src='{{$t->image}}'></li>
                    @else
                        <li> - </li>
                @endif
            @endforeach
        </ul>


    </div>
</div>
</body>
</html>
